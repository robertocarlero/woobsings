import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { UsersService } from 'src/services/users/users.service';

@Controller('users')
export class UsersController {
    constructor(public usersService: UsersService){}
    @Post()
    public async create(@Body() data: any) {
        
        return await this.usersService.createUser(data).then(() =>{
            return {
              code: 200,
              message: 'The user has been created',
            }
          }).catch((error) => {
            return {
              code: 500,
              message: 'The user cant been created',
            }
        });
    }

    @Get()
    public async findAll(@Query() query: any) {
        try {
            let data = await this.usersService.getAllUsers(query);
            return {
                code: 200,
                data: data,
            }
        } catch (error) {
            return {
                code: 500,
                message: 'Has been a error'
            }
        }
       
    }

    @Get(':id')
    public findOne(@Param('id') id: string) {
        return this.usersService.getUser(id)
    }

    @Put(':id')
    public update(@Param('id') id: string, @Body() data: any) {
        return this.usersService.updateUser(id, data)
    }

    @Delete(':id')
    public remove(@Param('id') id: string) {
        return this.usersService.deleteUser(id)
    }
}
