import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  	providedIn: 'root'
})
export class UsersService {
	public apiURL = 'http://localhost:3000/';

  	constructor(private http: HttpClient) {
		this.http.get(this.apiURL, {responseType: 'text'}).subscribe((res: any) => {
			console.log(res);
		});
	}

    public createUser(user: any): any {
		return new Promise((resolve) => {
			this.http.post(`${this.apiURL}users`, user).subscribe((res) => {
				console.log('respondio', res)
				resolve(res);
			});
		});
    }

    public getAllUsers(params: any): any {
		return new Promise((resolve) => {
			this.http.get(`${this.apiURL}users?limit=${params.limit}`).subscribe((res) => {
				resolve(res);
			});
		});
    }

    public getUser(id: string): any {
		return new Promise((resolve) => {
			this.http.get(`${this.apiURL}users/${id}`).subscribe((res) => {
				resolve(res);
			});
		});
    }

    public updateUser(id: string, data: any): any {
		return new Promise((resolve) => {
			this.http.put(`${this.apiURL}users/${id}`, data).subscribe((res) => {
				resolve(res);
			});
		});
    }
    
    public deleteUser(id: string): any{
		return new Promise((resolve) => {
			this.http.delete(`${this.apiURL}users/${id}`).subscribe((res) => {
				resolve(res);
			});
		});
    }
}
