import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		MatListModule,
		MatIconModule,
		MatDialogModule,
		MatFormFieldModule,
	],
	exports: [
		MatListModule,
		MatIconModule,
		MatDialogModule,
		MatFormFieldModule,
	]
})
export class MaterialModulesModule { }
